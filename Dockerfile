FROM java:openjdk-8u72-jdk

RUN apt-get update && apt-get install -y graphviz

COPY schemaSpy_5.0.0.jar /schemaSpy.jar
COPY mysql-connector-java-5.1.38-bin.jar /mysql-connector-java-5.1.38-bin.jar

VOLUME ["/output"]

CMD java -jar /schemaSpy.jar -t mysql -dp /mysql-connector-java-5.1.38-bin.jar -host $DB_HOST -db $DB_NAME -u $DB_USER -p $DB_PASSWORD -o /output

