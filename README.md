# Docker SchemaSpy for MySQL

## Environment variables:

- DB_HOST
- DB_NAME
- DB_USER
- DB_PASSWORD

__Build image__:
```
$ docker build -t schemaspy-mysql .
```

__Run image__:
```
$ docker run -it --rm -e "DB_HOST=localhost" -e "DB_NAME=db" -e "DB_USER=root" -e "DB_PASSWORD=toor" -v /tmp/schemaspy:/output --net="host" schemaspy-mysql
```



